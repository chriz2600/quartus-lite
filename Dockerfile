FROM ubuntu:22.04

# basic packages
RUN apt-get update && apt-get -y install expect locales wget libtcmalloc-minimal4 libglib2.0-0 && \
    echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen en_US.UTF-8 && \
    /usr/sbin/update-locale LANG=en_US.UTF-8

# adding scripts
ADD files/ /

# install quartus prime
# 19.1.0 http://download.altera.com/akdlm/software/acdsinst/19.1std/670/ib_tar/Quartus-lite-19.1.0.670-linux.tar
RUN mkdir -p /root/quartus && \
    cd /root/quartus && \
    wget -q https://downloads.intel.com/akdlm/software/acdsinst/23.1std.1/993/ib_tar/Quartus-lite-23.1std.1.993-linux.tar && \
    tar xvf Quartus-lite-23.1std.1.993-linux.tar && \
    /root/setup 23.1 && rm -rf /root/quartus && rm -rf /root/setup*
